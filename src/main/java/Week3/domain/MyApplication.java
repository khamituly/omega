package Week3.domain;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class MyApplication {

    private ArrayList<User> list_of_user;
    private int size = 0;

    public MyApplication(){
        list_of_user = new ArrayList<User>();
    }

    public void Extract() throws FileNotFoundException {
        File file = new File("C:\\Users\\Khamituly\\IdeaProjects\\Omega\\src\\main\\java\\Week3\\domain");
        Scanner sc = new Scanner(file);

        int id;
        String password, name, surname, username, prof;
        while(sc.hasNext())

        {
            id = sc.nextInt();
            name = sc.next();
            surname = sc.next();
            username = sc.next();
            password = sc.next();
            prof = sc.next();

            addUser(new User(id, name, surname, username, password,prof));
        }
    }

    public int getSize() {
        return size;
    }

    public ArrayList<User> getList_of_user() {
        return list_of_user;
    }



    public void addUser(User x) throws FileNotFoundException {
        if (size == 0) {
            User null_user = new User(0, "Name", "No", "NoName", "Qwerty123","programmer");
            list_of_user.add(null_user);
        } else
            list_of_user.add(x);
        size++;
        String usr = x.getUseraName();
        if( checkUser(usr) > 0){
            try(FileWriter writer = new FileWriter("C:\\Users\\Khamituly\\IdeaProjects\\Omega\\src\\main\\java\\Week3\\domain", false))
            {
                writer.write(x.getId()+" "+x.getName() + " " + x.getSurname() + " " +x.getUseraName()+ " " + x.getPassword());
                writer.flush();
                writer.close();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }else{
            return;
        }
    }

    public int getId_gen() {
        User y = list_of_user.get(size - 1);
        int id = y.getId();
        return id;
    }

    public int checkUser(String y) throws FileNotFoundException {
        MyApplication sc = new MyApplication();

        for (int i = 0; i < sc.getSize(); i++) {
            User x = sc.getList_of_user().get(i);
            if (y == x.getUseraName()) {
                return i;
            }
        }
        return 0;
    }

    public void Account(int id) {
        User x = list_of_user.get(id);
        System.out.println(" Your Identity document : " + x.getId());
        Scanner sc = new Scanner(System.in);
        x = list_of_user.get(id);
        System.out.println(x.getProfession());
        while (true) {
            System.out.println("1 - View all user");
            System.out.println("2 - Log out");
            int chosie = sc.nextInt();
            if (chosie == 1) {
                User user;
                for (int i = 0; i < size; i++) {
                    user = list_of_user.get(i);
                    System.out.println(user.getName() + " " + user.getSurname());
                }
            }else{
                break;
            }
            Account(id);
        }
    }


    public void SignUp() throws FileNotFoundException {
        System.out.println("Please enter the name: ");
        User new_user = new User();
        Scanner sc = new Scanner(System.in);
        String x = sc.nextLine();
        new_user.setName(x);
        System.out.println("Enter the Surname: ");
        x = sc.nextLine();
        new_user.setSurname(x);
        System.out.println("Create the username: ");
        x = sc.nextLine();
        System.out.println("Enter the profession(experience , salary, position)");
        x = sc.nextLine();
        if (new_user.checkUserName(x)) {
            new_user.setUserName(x);
        } else {
            System.out.println("Error on line 87, this username already exists!");
            SignUp();
        }
        System.out.println("Create the password :\n (should consist of at least two digit number and uppercase symbol) ");
        x = sc.nextLine();
        Password password = new Password();
        if (Password.checkPassword(x)) {
            new_user.setPassword(x);
        } else {
            System.out.println("Error on line 96, invalid password form! ");
            SignUp();
        }
        addUser(new_user);
        System.out.println("Access!");
        Account(size);
    }

    public void SignIn() throws FileNotFoundException {
        Scanner sc = new Scanner(System.in);
        while (true) {
            String usrn;
            System.out.println("Enter your username: ");
            usrn = sc.nextLine();
            int position = checkUser(usrn);
            if (position == 0) {
                System.out.println("Error on line 62! \n there no such user or your entered  the username incorrectly!");
            } else {
                System.out.println("Entre the password: ");
                usrn = sc.nextLine();
                MyApplication app = new MyApplication();
                User x = app.getList_of_user().get(position);
                if (usrn != x.getPassword()) {

                    System.out.println("Error on line 69! \n incorectly password! ");
                } else {
                    System.out.println("Access!");
                    Account(position);
                }
            }
        }
    }

    public void menu() throws FileNotFoundException {
        Extract();
        Scanner sc = new Scanner(System.in);
        while (true) {
            System.out.println("1 - Sign In");
            System.out.println("2 - Sign Up ");
            System.out.println("3 - Exit \n Input the option: ");
            int a = sc.nextInt();
            if (a == 1) {
                SignIn();
            } else if (a == 2) {
                SignUp();
            } else {
                break;
            }

        }
    }

}

