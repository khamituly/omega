package Week3.domain;


public class Password {
    private String password;

    public void setPassword(String new_pass)
    {
        this.password = new_pass;

    }

    public String getPassword(){
        return password;
    }

    public static  boolean checkPassword(String new_password){


        int count_digit = 0, countUppercase = 0;

        char c;
        for (int  i=0; i < new_password.length(); i++) {
            c = new_password.charAt(i);
            if (c >= '0' && c <= '9') {
                count_digit++;
            }
        }

        for(int i=0; i<new_password.length(); i++){
            if(Character.isUpperCase(new_password.charAt(i))){
                countUppercase++;
            }
        }

        if((count_digit >= 2 && countUppercase >= 2))
            return true;
        else return false;

    }

}
