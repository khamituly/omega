package Week3.domain;

public class Position extends Profession {
    private String pos;
    private int experience;
    private int salary;

    Position(){}

    Position(String profession){
        super(profession);
    }

    Position(String profession, int salary, int expirience, String positon){
        super(profession);
        setExperience(expirience);
        setPos(positon);
        setSalary(salary);
    }

    public void setPos(String x){
        this.pos = x;
    }

    public void setExperience(int x){
        this.experience = x;
    }

    public void setSalary(int x) {
        this.salary = x;
    }

    public int getExperience(){
        return experience;
    }

    public int getSalary() {
        return salary;
    }

    public String getPos(){
        return pos;
    }

    @Override
    public String toString() {
        return super.toString()+", position :"+pos+", expirience: "+experience+", salary: " +salary;
    }
}
